# eduNEXT Coding Challenge

In this repository you will find the description of two challenges.

## For Devs: Back end

The developer challenge is designed to give us an overview on your dexterity when developing web applications based on python as the underlying backend language with the usage of django or flask as the application framework of choice.

## For Devs: Front end

The developer challenge is designed to give us an overview on your dexterity when creating rich applications that run in the browser and interact with a server side api using modern front end technologies for both functionality and style.

## For DevOps

The devops challege is geared towards testing your ability to manage infrastructure and deployment of web applications in an automated way that allows us to obtain the high reliability that we offer our customers and also to iterate fast in our development.

# Assessment

The main objective of this challenge is not only to determine whether you have the technical knowledge, but also to try and find what motivates you and how you find creative and elegant solutions to meet the requirements.

We are looking for someone who is a good fit for our team, and this requires that candidates alongside their code and technical skill are also motivated to work and lead projects and features into completion, and that they are capable to communicate effectively with other members of the team and customers about the requirements, the specifications or the status of the work.

We will look at the results of this challenge through the lens of:

- Communication
    + Clear and complete
- Independence of work
- Tech skills & best practices
    + Correctness of the solution
    + Architecture of the solution
    + QA
        * Code quality
        * Unit testing


# Delivering

To share this code effectively with us and keep your work secure, we suggest that you create a free account on BitBucket and push the code to your private fork. Please share this fork with us:
- https://bitbucket.org/FelipeM_ntoya/
- https://bitbucket.org/juancamilom/

Then clone/push this repository into your own account and use it as the location to store the code.

Work freely and commit to the repository as you go.
Push to your repository

Communicate with us using the Jira Board or the Slack channel we will invite you to as a part of this challenge.


# Kudos

You will receive extra recognition for special achievements in the resolution of the challenge. In this category will fall: using a continuous integration or testing service like [CircleCI](https://circleci.com/), keeping your code organized in the repository using the [github-flow](https://guides.github.com/introduction/flow/), or having excellent communication at the jira board.

# Rules

Please keep this challenges to yourself, we expect you to be the only author of the code that is published here.