# eduNEXT Coding Challenge


## Situation description

For each of our customers, we keep a configuration object stored in the database. This configuration object is something we can interact with using a REST api that exposes the json for the user and other applications.

An example of the object we store and access for each customer is:

```
{
    "ENABLE_MKTG_SITE": false,
    "LANGUAGE_CODE": "en-US",
    "PLATFORM_NAME": "eduNEXT Academy",
    "SITE_NAME": "demo.edunext.io",
    "TIME_ZONE_DISPLAYED_FOR_DEADLINES": "America/Bogota",
    "LAST_ACCESSED": "Tue Jun 13 08:19:44 CEST 2017",
    "course_about_show_social_links": false,
    "course_index_overlay_logo_file": "/static/images/logo.png",
    "course_org_filter": "eneXT",
    "released_languages": "es-419,en-US",
    "show_homepage_promo_video": false,
    "show_partners": false
}
```


## Challenge

Fork this repo and use it to create a nice front end application that allows the user to look at the variables we have stored, and also change those that have the name written in ALL_CAPS.

This application will serve as the users settings page, so you can draw inspiration from your favorite settings page.


## Scaffold

To get you started on the challenge quickly, we have created an application you can interact with as you develop your code. This application will create the backend server with the API, so you can focus only on what happens in the browser.

To use the scaffold application, go into the scaffold directory, create a virtualenv[^1] and run bootstrap target with make.

```
cd scaffold
virtualenv venv
source venv/bin/activate
make bootstrap
make run
```

Now you can go to `http://localhost:8010/static/hello.html` where a basic html placeholder is waiting for you.

The server side API will be at: `http://localhost:8010/api/v1/customerdata/`.



[^1]:
Virtualenv is a python utility to make development simple.

A guide on how to install virtualenv for Linux, Mac and Windows is available here (no need to do the django part):
http://pythoncentral.io/how-to-install-python-django-windows-mac-linux/

Disclaimer: this instructions were tested using a linux OS, if you have problems running this in a different OS, please let us know.