const edunext = {
  language: window.navigator.language,
  init: function() {
    $.fn.extend({//funcion animaciones principales.
      animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
          $(this).removeClass('animated ' + animationName);
        });
      }
    });
    $('.set-profile').on('click', e => {
      edunext.active();
    });
    $('.close-update').on('click', e => {
      edunext.close();
    });
    $('.save').on('click', e => {
      edunext.save();
    });

  },
  setLoading: function(text = 'Cargando...', loader = true, time = false) {
    let feedback = '';
    let language = (edunext.language === 'es-419') ? "Cargando..." : "Loading...";
    let loaderbody = '';
    (text === 'Cargando...') ? feedback = language : feedback = text;
    (loader == true) ? loaderbody = `<div style="margin-left: 5px; width: 35px; height: 35px;" class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>` : ``;
    (time == false) ? Materialize.toast(`${feedback} ${loaderbody}`) : Materialize.toast(`${feedback} ${loaderbody}`, time)
  },
  hideLoading: function() {
    ($("#toast-container").length == 1) ? $("#toast-container").remove() : null;
  },
  loading: function (status = true){
    status ? edunext.setLoading() : edunext.hideLoading();
  },
  entries : function* (obj) {
    for (let key of Object.keys(obj)) {
      yield [key, obj[key]];
    }
  },
  findInput : function(key, value){
    return new Promise((resolve, reject) => {
      let newKey = key.replace(/_/g , "-");
      ($(`#${newKey}`).length == 1) ? $(`#${newKey}`).val(value) : null;
      resolve(true);
    });
  },
  findDiv: function(key, value){
    return new Promise((resolve, reject) => {
      let newKey = key.replace(/_/g , "-");
      ($(`.${newKey}`).length == 1) ? $(`.${newKey}`).text(value) : null;
        resolve(true);
    });
  },
  active : function() {
    $(".shadow-data").addClass("show");
    $(".container-data").animateCss("slideInUp");
  },
  close : function () {
    $('.container-data').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
  		$(".shadow-data").removeClass("show");
  	});
  	$(".container-data").animateCss("slideOutDown");
  },
  prepare: function(eduNextId, storage, key, value) {
    return new Promise((resolve, reject) => {
      let prepareJSON = {}
      prepareJSON.eduNextId = eduNextId
      prepareJSON.data = {}
      for (let [key, value] of edunext.entries(storage)) {
        let newKey = key.replace(/_/g , "-");
        ($(`#${newKey}`).length == 1) ? prepareJSON.data[key] = $(`#${newKey}`).val() : prepareJSON.data[key] = storage[key]
      }
      resolve(prepareJSON);
    });
  },
  save : () => {
    edunext.loading(true);
    let eduNextId = localStorage.getItem('eduNextId');
    let storage = JSON.parse(localStorage.getItem('eduNextData'));
    // (key === key.toUpperCase()) ? edunext.send(key, value).then(data => { api.put(data).then(status => { edunext.hideLoading() }); }).catch(error => { }) : null
    edunext.prepare(eduNextId, storage).then(
      data => {
         api.data.put(data).then(
           status => {
             edunext.loading(false);
             edunext.setLoading('Your setting are complete upgrade',false,5000);
             edunext.close();
         })
         .catch(
           error => {
             edunext.loading(false);
             edunext.setLoading(error,false,8000);
           })
        });
  }
}
