const baseUrl = '/api/v1/customerdata/';
const api = {
  data: {
    async getData(){
      let data =  await axios.get(`${baseUrl}`);
      return  data.data[0];
    },
    put(data){
      console.log(data);
      console.log(data.data);
      return new Promise((resolve, reject) => {

        axios.put(`${baseUrl}${data.eduNextId}/`,
          {
            'id' : data.eduNextId,
            'data': data.data
          },
          {
            headers: {
              'Content-Type': 'application/json'
            }
          }
        )
        .then((response) =>{
          resolve(true);
        })
        .catch((error) => {
          reject(`${error.response.status}: ${error.response.statusText}`);
        });
      });
    },
  },
  start : {
    starts() {
      edunext.init();
      api.data.getData().then(data => {
        localStorage.setItem('eduNextId', data.id);
        localStorage.setItem('eduNextData', JSON.stringify(data.data));
        api.setInfo(data.data);
      }).catch(error => {
        // handle an error here (maybe try to reconnect conecction with API after a few seconds)
      });
    }
  },
  setInfo : info => {
    edunext.loading(true);
    for (let [key, value] of edunext.entries(info)) {
      (key === key.toUpperCase()) ? edunext.findInput(key,value) : edunext.findDiv(key, value).then(status => { edunext.loading(false); })
    }
  }
};
api.start.starts();
