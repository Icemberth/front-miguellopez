# Service example

This application is a sample REST API provided for the eduNEXT developer challenge.

The application you develop should interact with this app as it would do with other applications on our production environment.
