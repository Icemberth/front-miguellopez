# eduNEXT Coding Challenge


## Situation description

For each of our customers, we keep a configuration object stored in the database. This configuration object is something we can interact with using a REST api that exposes the json for the user and other applications.

An example of the object we store and access for each customer is:

```
{
    "ENABLE_MKTG_SITE": false,
    "LANGUAGE_CODE": "en-US",
    "PLATFORM_NAME": "eduNEXT Academy",
    "SITE_NAME": "demo.edunext.io",
    "TIME_ZONE_DISPLAYED_FOR_DEADLINES": "America/Bogota",
    "LAST_ACCESSED": "Tue Jun 13 08:19:44 CEST 2017",
    "course_about_show_social_links": false,
    "course_index_overlay_logo_file": "/static/edunext/images/logo.png",
    "course_org_filter": "eneXT",
    "released_languages": "es-419,en-US",
    "show_homepage_promo_video": false,
    "show_partners": false
}
```


## Challenge

For the challenge, please create a backend application using flask or django that interacts with the REST API and allows server application running in the same network to read the properties which have lowcase names, and read/edit those with uppercase names.

## Scaffold

To get you started on the challenge quickly, we have created an application you can interact with as you develop your code. This application run in a separate process and its a simple API that stores some data in the right form.

To use the scaffold application, go into the scaffold directory, create a virtualenv and run bootstrap target with make.

```
cd scaffold
virtualenv venv/ && source venv/bin/activate
make bootstrap
```

Now you can point your own app to work using `http://localhost:8010/api/v1/customerdata/` as your REST API.

For this challenge, you need to create a separate django or flask application able to integrate to the scaffold application using RESTful api calls.
